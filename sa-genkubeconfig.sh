#!/bin/bash

function usage {
    echo ""
    echo "Create a kubeconfig file for input ServiceAccount."
    echo ""
    echo "usage: $0 --service-account-name string --namespace string --context string"
    echo ""
    echo "  --service-account-name  string   (Mandatory) - Service Account from which to generate the kubeconfig file"
    echo "                                   (example: my-namespace-sa)"
    echo "  --namespace string               (Optional) - Namespace of the ServiceAccount in the cluster "
    echo "                                   (example: my-namespace)"
    echo "  --context string                 (Optional) - Context to use to generate the kubeconfig"
    echo "                                   (example: - admin)"
    echo "  --output-path string             (Optional) - Path where the gen file is created"
    echo "                                   (default: CURRENT_DIR/NAMESPACE_NAME/SERVICE_ACCOUNT_NAME)"
    echo "  --log-status string              (Optional) - default is off"
    echo "                                   (log-status on)"
    echo ""
}

function log {
    if [[ $LOG_STATUS == "on" ]] ; then
      echo "$1";
    fi
}

function die {
    printf "Script failed: %s\n\n" "$1"
    exit 1
}
 
while [ $# -gt 0 ]; do
   if [[ $1 == "--help" ]]; then
        usage
        exit 0
    elif [[ $1 == "--"* ]]; then
        param_name="${1/--/}"
        parsed_param_name=${param_name//[-]/_} 
        declare "${parsed_param_name^^}"="$2"
        shift
    fi
    shift
done

#ServiceAccount for which generate the kubeconfig - Mandatory
if [[ -z "$SERVICE_ACCOUNT_NAME" ]] ; then
    usage
    die "Missing parameter --service_account_name"
else
    log "SERVICE ACCOUNT NAME: $SERVICE_ACCOUNT_NAME"
fi

#Save original context before switch if needed
ORIGINAL_CURRENT_CONTEXT=$(kubectl config current-context)
#Context used to generate the kubeconfig -OPTIONAL (alternatively current-context is used)
if [[ -z "$CONTEXT" ]] ; then
    log "NO CONTEXT FOUND"
    log "USE CURRENT CONTEXT: "
    CONTEXT=$(kubectl config current-context)
    log $CONTEXT
else
    log "FOUND INPUT CONTEXT: $CONTEXT";
    kubectl config use-context $CONTEXT 1>/dev/null
    log "USE INPUT CONTEXT: $CONTEXT"
fi

if [[ -z "$NAMESPACE" ]] ; then
    log "NO INPUT NAMESPACE FOUND FOR SERVICE ACCOUNT: $SERVICE_ACCOUNT_NAME"
    NAMESPACE=$(kubectl get sa -o=jsonpath='{.items[0]..metadata.namespace}')
    log "USE CURRENT NAMESPACE: $NAMESPACE"
else
    log "USE INPUT NAMESPACE: $NAMESPACE";
fi

if [[ -z "$OUTPUT_PATH" ]] ; then
    log "NO OUTPUT_PATH FOUND, USE DEFAULT:"
    OUTPUT_PATH="$PWD/$NAMESPACE/$SERVICE_ACCOUNT_NAME"
    log "$OUTPUT_PATH";
else
    log "FOUND OUTPATH - USE: $OUTPUT_PATH";
fi

mkdir -p $OUTPUT_PATH 1>/dev/null
##########################SA KUBECONFIG GENERATION#####################################
log "START PROJECT KUBECONF FILE GENERATION ..."

NEW_CONTEXT=$SERVICE_ACCOUNT_NAME
KUBECONFIG_FILE="$SERVICE_ACCOUNT_NAME-kubeconfig"
KUBECONFIG_FILE_PATH="$OUTPUT_PATH/$KUBECONFIG_FILE"

SECRET_NAME=$(kubectl get serviceaccount ${SERVICE_ACCOUNT_NAME} --context ${CONTEXT} --namespace ${NAMESPACE} -o jsonpath='{.secrets[0].name}')

if [[ -z "$SECRET_NAME" ]] ; then
    die "IMPOSSIBLE TO RETRIEVE SERVICE ACCOUNT SECRET NAME"
fi
#####################################################################

#GET SA SECRET TOKEN DATA
TOKEN_DATA=$(kubectl get secret ${SECRET_NAME} --context ${CONTEXT} --namespace ${NAMESPACE} -o jsonpath='{.data.token}')
if [[ -z "$TOKEN_DATA" ]] ; then
    die "IMPOSSIBLE TO RETRIEVE SECRET TOKEN DATA OF SERVICE ACCOUNT $SERVICE_ACCOUNT_NAME WITH SECRET NAME: $SECRET_NAME"
fi

TOKEN=$(echo ${TOKEN_DATA} | base64 -d)
if [[ -z "$TOKEN" ]] ; then
    die "IMPOSSIBLE TO CONVERT TOKEN TO BASE64"
fi

#CREATE A FULL COPY
kubectl config view --raw > "$KUBECONFIG_FILE_PATH.full.tmp"

#SWITCH CONTEXT
kubectl --kubeconfig "$KUBECONFIG_FILE_PATH.full.tmp" config use-context $CONTEXT 1>/dev/null

#CREATE MINIFIED COPY
kubectl --kubeconfig "$KUBECONFIG_FILE_PATH.full.tmp" config view --flatten --minify > "$KUBECONFIG_FILE_PATH.tmp"

#RENAME CONTEXT
kubectl config --kubeconfig "$KUBECONFIG_FILE_PATH.tmp" rename-context "$CONTEXT" "$NEW_CONTEXT" 1>/dev/null

#SET CREDENTIAL WITH TOKEN
kubectl config --kubeconfig "$KUBECONFIG_FILE_PATH.tmp" set-credentials "$NEW_CONTEXT-token-user" --token "$TOKEN" 1>/dev/null

#SET USER FOR NEW CONTEXT
kubectl config --kubeconfig "$KUBECONFIG_FILE_PATH.tmp" set-context "$NEW_CONTEXT" --user "$NEW_CONTEXT-token-user" 1>/dev/null

#SET NAMESPACE FOR NEW CONTEXT
kubectl config --kubeconfig "$KUBECONFIG_FILE_PATH.tmp" set-context "$NEW_CONTEXT" --namespace "$NAMESPACE" 1>/dev/null

#SAVE NEW CONFIG TO FINAL YAML PROJECT KUBECONF FILE
kubectl config --kubeconfig "$KUBECONFIG_FILE_PATH.tmp" view --flatten --minify > "$KUBECONFIG_FILE_PATH.yaml"

#REMOVE TEMP FILE
rm "$KUBECONFIG_FILE_PATH.full.tmp" "$KUBECONFIG_FILE_PATH.tmp" 1>/dev/null

#VERIFY CONTEXT
#kubectl config --kubeconfig "$KUBECONFIG_FILE_PATH.yaml" get-contexts

#CREATE JSON CERSION OF PROJECT KUBECONF FILE
kubectl --kubeconfig "$KUBECONFIG_FILE_PATH.yaml" config view --raw --output='json' > "$KUBECONFIG_FILE_PATH.json"

log "RESTORE ORGINAL CURRENT CONTEXT: $ORIGINAL_CURRENT_CONTEXT"
kubectl config use-context $ORIGINAL_CURRENT_CONTEXT 1>/dev/null