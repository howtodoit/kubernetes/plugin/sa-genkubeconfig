## sa-genkubeconfig k8s kubectl plugin

### Description
This plugin allow to generate the kubeconfig file for a Service Account, output format is yaml and json. Common scenario is the use for the kubernetes cluster connection in a Continuous Delivery scenario and also using kubectl tool config option, following the principle of least privilege.

### Plugin Installation
```bash

# "install" your plugin by moving it to a directory in your $PATH 
mv ./sa-genkubeconfig.sh /usr/local/bin/kubectl-sa-genkubeconfig
chmod +x /usr/local/bin/kubectl-sa-genkubeconfig

# check that kubectl recognizes your plugin
kubectl plugin list
```
### Plugin usage
Print usage info:
```bash 
kubectl sa genkubeconfig --help
```
Generate kubeconfig file for input ServiceAccount in current namespace:
```bash 
kubectl sa genkubeconfig --service-account-name myserviceaccount
```
Generate kubeconfig file for input ServiceAccount and input namespace and log enabled:
```bash 
kubectl sa genkubeconfig --service-account-name myserviceaccount --namespace 'mynamespace' --log-status on
```
Generate kubeconfig file for input ServiceAccount using input config context :
```bash 
kubectl sa genkubeconfig --service-account-name myserviceaccount --context 'mycontext'
```

## Authors and acknowledgment
Vincenzo Santucci - vincenzo.santucci@gmail.com
